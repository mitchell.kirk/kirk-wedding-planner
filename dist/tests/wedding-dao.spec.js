"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var entities_1 = require("../src/entities");
var entities_2 = require("../src/entities");
var connection_1 = require("../src/connection");
var wedding_service_impl_1 = require("../src/services/wedding-service-impl");
var wedddingService = new wedding_service_impl_1.WeddingServiceImpl();
test("Create a wedding", function () { return __awaiter(void 0, void 0, void 0, function () {
    var testWedding, result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                testWedding = new entities_2.Wedding(99, "2022-02-02", "test location", "test name", 9999);
                return [4 /*yield*/, wedddingService.registerWedding(testWedding)];
            case 1:
                result = _a.sent();
                expect(result.weddingId).toBe(99);
                return [2 /*return*/];
        }
    });
}); });
test("Create an expense", function () { return __awaiter(void 0, void 0, void 0, function () {
    var testExpense, result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                testExpense = new entities_1.Expense(100, 99, "test reason", 100);
                return [4 /*yield*/, wedddingService.registerExpense(testExpense)];
            case 1:
                result = _a.sent();
                expect(result.expenseId).toBe(100);
                return [2 /*return*/];
        }
    });
}); });
test("Edit a wedding", function () { return __awaiter(void 0, void 0, void 0, function () {
    var editWedding, result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                editWedding = new entities_2.Wedding(99, "2023-03-03", "edit wedding", "edit name", 1000);
                return [4 /*yield*/, wedddingService.modifyWedding(editWedding)];
            case 1:
                result = _a.sent();
                expect(result.budget).toBe(1000);
                return [2 /*return*/];
        }
    });
}); });
test("Edit an expense", function () { return __awaiter(void 0, void 0, void 0, function () {
    var editExpense, result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                editExpense = new entities_1.Expense(100, 99, "edit reason", 200);
                return [4 /*yield*/, wedddingService.modifyExpense(editExpense)];
            case 1:
                result = _a.sent();
                expect(result.amount).toBe(200);
                return [2 /*return*/];
        }
    });
}); });
test("Get wedding by Id", function () { return __awaiter(void 0, void 0, void 0, function () {
    var weddingId, result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                weddingId = 99;
                return [4 /*yield*/, wedddingService.retrieveWeddingById(weddingId)];
            case 1:
                result = _a.sent();
                expect(result.budget).toBe(1000);
                return [2 /*return*/];
        }
    });
}); });
test("Get expense by Id", function () { return __awaiter(void 0, void 0, void 0, function () {
    var expenseId, result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                expenseId = 100;
                return [4 /*yield*/, wedddingService.retrieveExpenseById(expenseId)];
            case 1:
                result = _a.sent();
                expect(result.weddingId).toBe(99);
                return [2 /*return*/];
        }
    });
}); });
test("Get all weddings", function () { return __awaiter(void 0, void 0, void 0, function () {
    var result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, wedddingService.retrieveAllWeddings()];
            case 1:
                result = _a.sent();
                expect(result).not.toBe(null);
                return [2 /*return*/];
        }
    });
}); });
test("Get all expenses by wedding Id", function () { return __awaiter(void 0, void 0, void 0, function () {
    var weddingId, result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                weddingId = 99;
                return [4 /*yield*/, wedddingService.retrieveAllExpensesById(weddingId)];
            case 1:
                result = _a.sent();
                expect(result).not.toBe(null);
                return [2 /*return*/];
        }
    });
}); });
test("Get all expenses", function () { return __awaiter(void 0, void 0, void 0, function () {
    var result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, wedddingService.retrieveAllExpenses()];
            case 1:
                result = _a.sent();
                expect(result).not.toBe(null);
                return [2 /*return*/];
        }
    });
}); });
test("Delete expense by Id", function () { return __awaiter(void 0, void 0, void 0, function () {
    var expenseId, result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                expenseId = 100;
                return [4 /*yield*/, wedddingService.removeExpense(expenseId)];
            case 1:
                result = _a.sent();
                expect(result).toBe(true);
                return [2 /*return*/];
        }
    });
}); });
test("Delete wedding by Id", function () { return __awaiter(void 0, void 0, void 0, function () {
    var weddingId, result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                weddingId = 99;
                return [4 /*yield*/, wedddingService.removeWedding(weddingId)];
            case 1:
                result = _a.sent();
                expect(result).toBe(true);
                return [2 /*return*/];
        }
    });
}); });
afterAll(function () {
    connection_1.client.end();
});
