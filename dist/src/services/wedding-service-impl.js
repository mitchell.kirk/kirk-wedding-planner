"use strict";
exports.__esModule = true;
exports.WeddingServiceImpl = void 0;
var wedding_dao_postgres_1 = require("./daos/wedding-dao-postgres");
var WeddingServiceImpl = /** @class */ (function () {
    function WeddingServiceImpl() {
        this.weddingDAO = new wedding_dao_postgres_1.WeddingDaoPostgres();
        this.expenseDAO = new wedding_dao_postgres_1.WeddingDaoPostgres();
    }
    WeddingServiceImpl.prototype.registerWedding = function (wedding) {
        return this.weddingDAO.createWedding(wedding);
    };
    WeddingServiceImpl.prototype.registerExpense = function (expense) {
        return this.expenseDAO.createExpense(expense);
    };
    WeddingServiceImpl.prototype.retrieveAllWeddings = function () {
        return this.weddingDAO.getAllWeddings();
    };
    WeddingServiceImpl.prototype.retrieveWeddingById = function (weddingId) {
        return this.weddingDAO.getWeddingById(weddingId);
    };
    WeddingServiceImpl.prototype.retrieveAllExpenses = function () {
        return this.expenseDAO.getAllExpenses();
    };
    WeddingServiceImpl.prototype.retrieveAllExpensesById = function (weddingId) {
        return this.expenseDAO.getAllExpensesById(weddingId);
    };
    WeddingServiceImpl.prototype.retrieveExpenseById = function (expenseId) {
        return this.expenseDAO.getExpenseById(expenseId);
    };
    WeddingServiceImpl.prototype.modifyWedding = function (wedding) {
        return this.weddingDAO.updateWedding(wedding);
    };
    WeddingServiceImpl.prototype.modifyExpense = function (expense) {
        return this.expenseDAO.updateExpense(expense);
    };
    WeddingServiceImpl.prototype.removeWedding = function (weddingId) {
        return this.weddingDAO.deleteWeddingById(weddingId);
    };
    WeddingServiceImpl.prototype.removeExpense = function (expenseId) {
        return this.expenseDAO.deleteExpenseById(expenseId);
    };
    return WeddingServiceImpl;
}());
exports.WeddingServiceImpl = WeddingServiceImpl;
