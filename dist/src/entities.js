"use strict";
exports.__esModule = true;
exports.Expense = exports.Wedding = void 0;
var Wedding = /** @class */ (function () {
    function Wedding(weddingId, date, location, name, budget) {
        this.weddingId = weddingId;
        this.date = date;
        this.location = location;
        this.name = name;
        this.budget = budget;
    }
    return Wedding;
}());
exports.Wedding = Wedding;
var Expense = /** @class */ (function () {
    function Expense(expenseId, weddingId, reason, amount) {
        this.expenseId = expenseId;
        this.weddingId = weddingId;
        this.reason = reason;
        this.amount = amount;
    }
    return Expense;
}());
exports.Expense = Expense;
