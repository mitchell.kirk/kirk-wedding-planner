import { Wedding } from "../entities";
import { Expense } from "../entities";


export default interface WeddingService{
    //create
    registerWedding(wedding:Wedding):Promise<Wedding>
    registerExpense(expense:Expense):Promise<Expense>

    //read
    retrieveAllWeddings():Promise<Wedding[]>;
    retrieveWeddingById(weddingId:number):Promise<Wedding>;
    retrieveAllExpensesById(weddingId:number):Promise<Expense[]>;
    retrieveAllExpenses():Promise<Expense[]>;
    retrieveExpenseById(expenseId:number):Promise<Expense>;

    //modify
    modifyWedding(wedding:Wedding):Promise<Wedding>
    modifyExpense(expense:Expense):Promise<Expense>    

    //delete
    removeWedding(weddingId:number):Promise<boolean>
    removeExpense(expense:number):Promise<boolean>

}