import { Expense, Wedding } from "../../entities";



export interface WeddingDAO{

    //create
    createWedding(wedding:Wedding):Promise<Wedding>;

    //read
    getAllWeddings():Promise<Wedding[]>;
    getWeddingById(weddingId:number): Promise<Wedding>;

    //update
    updateWedding(wedding:Wedding):Promise<Wedding>;

    //delete
    deleteWeddingById(weddingId:number):Promise<boolean>;
    
}

export interface ExpenseDAO{

    //create
    createExpense(expense:Expense):Promise<Expense>;

    //read
    getAllExpenses():Promise<Expense[]>;
    getAllExpensesById(weddingId:number):Promise<Expense[]>;
    getExpenseById(expenseId:number): Promise<Expense>;

    //update
    updateExpense(expense:Expense):Promise<Expense>;

    //delete
    deleteExpenseById(expenseId:number):Promise<boolean>;
    
}