export class MissingResourceError{

    message:string;
    description:string = "Sorry, the resource you requested could not be found";

    constructor(message:string){
        this.message = message;
    }
}

export class CreateDataError{

    message:string;
    description:string = "Sorry, there was an error creating the data you requested";

    constructor(message:string){
        this.message = message;
    }
}